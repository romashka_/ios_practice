//
//  BoolRequest.swift
//  WinsIOSPractice
//
//  Created by Роман Шуркин on 06.06.2020.
//  Copyright © 2020 Роман Шуркин. All rights reserved.
//

import Foundation

struct BoolRequest: Codable {
    
    var value: Bool?
    
    var id: String?
}
