//
//  PromocodeDTO.swift
//  WinsIOSPractice
//
//  Created by Роман Шуркин on 06.06.2020.
//  Copyright © 2020 Роман Шуркин. All rights reserved.
//

import Foundation

struct PromocodeDTO: Codable {
    
    var id: String
    
    var code: String
}
