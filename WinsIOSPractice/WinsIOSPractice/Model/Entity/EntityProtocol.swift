//
//  EntityProtocol.swift
//  WinsIOSPractice
//
//  Created by Роман Шуркин on 04.06.2020.
//  Copyright © 2020 Роман Шуркин. All rights reserved.
//

import Foundation
import RealmSwift

protocol EntityProtocol: Codable {
    var entity: Object { get }
}
