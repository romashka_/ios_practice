//
//  SignInProtocols.swift
//  WinsIOSPractice
//
//  Created by Роман Шуркин on 05.05.2020.
//  Copyright © 2020 Hope To. All rights reserved.
//

import Foundation

internal protocol SignInViewInput: class {
    func setUp()
}

internal protocol SignInViewOutput: class {
    func signIn(login: String, password: String)
    func createUser(login: String, password: String)
    func nextVC()
    func showSignAlert(text: String)
}

internal protocol SignInRouterInput:class {
    func nextVc()
    func signIn()
    func showSignAlert(text: String)
    func showLoader()
    func closeLoader()
}

internal protocol SignInInteractorInput:class {
    func signIn(login: String, password: String)
    func createUser(login: String, password: String)
}

internal protocol SignInInteractorOutput:class {
    
    func signIn()
    
    func closeLoader()
    
    func actionOfLoginNotOrigin()
    
    func actionOfIncorrectLoginAndPassword()
    
    func nextVCAfterRegistration()
    func nextVCAfterSignIn()
}
